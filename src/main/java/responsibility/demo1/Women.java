package responsibility.demo1;

public class Women implements IWomen {
    /*
    通过int类型描述女性的状态
    1:未婚
    2:已婚
    3:夫死
     */
    private int type = 0;

    //妇女的请示
    private String request = "";

    //通过构造函数传递属性
    public Women(int type, String request) {
        this.type = type;
        switch (this.type){
            case 1:
                this.request = "女儿的请求是" + request;
                break;
            case 2:
                this.request = "妻子的请求是" + request;
                break;
            case 3:
                this.request = "母亲的请求是" + request;
        }
    }

    public int getType() {
        return this.type;
    }

    public String getReuest() {
        return this.request;
    }
}
