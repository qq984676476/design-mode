package responsibility.demo1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 责任连模式
 */
public class Client {
    public static void main(String[] args){
        //产生几个女性
        Random random = new Random();
        List<IWomen> list = new ArrayList<IWomen>();
        for (int i = 0; i < 5; i++) {
            list.add(new Women(random.nextInt(4), "逛街" ));
        }
        //定义三个请示对象
        Handler father = new Father();
        Handler husband = new Husband();
        Handler son = new Son();

        //设置请求顺序
        father.setNextHandler(husband);
        husband.setNextHandler(son);
        for (IWomen c :list) {
            father.handlerMassage(c);
        }
    }

}
