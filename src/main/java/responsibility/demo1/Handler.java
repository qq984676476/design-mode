package responsibility.demo1;

public abstract class Handler {
    public final static int FATHER_LEVEL_REQUEST = 1;
    public final static int HUSBAND_LEVEL_REQUEST = 2;
    public final static int SON_LEVEL_REQUEST = 3;
    //能处理的级别
    private int level = 0;
    //下一个责任人是谁
    private Handler nextHandler;
    //每一个类都要说名自己能处理的级别
    public Handler(int level){
        this.level = level;
    }

    //处理请求
    public final void handlerMassage(IWomen women){
        if(women.getType() == this.level){
            this.response(women);
        }else{
            if(this.nextHandler != null){
                this.nextHandler.handlerMassage(women);
            }else{
                System.out.println("没人了");
            }
        }
    }
    //设置下一级审批人
    public void setNextHandler(Handler handler){
        this.nextHandler = handler;
    }

    //都要处理的方法,回应请示
    protected abstract void response(IWomen women);
}
