package prototype.demo1;

public class AdvTemplate {
    //广告信名称
    private String advName = "抽奖活动";

    //广告内容
    private String advContext = "抽奖啦,抽奖啦,抽奖啦,抽奖啦,抽奖啦";

    //获取广告的名称
    public String getAdvName(){
        return this.advName;
    }

    //获取广告的内容
    public String getAdvContext(){
        return this.advContext;
    }
}
