package prototype.demo1;

import java.util.Random;

public class Client {
    //发送的数量
    private static int MAX_COUNT = 6;

    public static void main(String[] args){
        //把模板定义出来
        Mail mail = new Mail(new AdvTemplate());
        mail.setTail("XXX版权所有");
        for (int i = 0; i < MAX_COUNT; i++) {
            //没封邮件的不同处
            mail.setApplication(getRandString(5) + "先生(女士)");
            mail.setReceiver(getRandString(5)+ "@" + getRandString(8) + ".com");
            sendMail(mail);
        }
    }

    public static String getRandString(int maxLength){
        String str = "abcdefghijklmnopqrskuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuffer sb = new StringBuffer();
        Random random = new Random();
        for (int i = 0; i < maxLength; i++) {
               sb.append(str.charAt(random.nextInt(str.length())));
        }
        return sb.toString();
    }

    public static void sendMail(Mail mail){
        System.out.println("标题:" + mail.getAdvName());
        System.out.println("收件人:" + mail.getReceiver());
        System.out.println("....");
        System.out.println("发送成功");
        System.out.println("");
    }
}
