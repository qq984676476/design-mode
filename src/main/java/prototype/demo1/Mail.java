package prototype.demo1;

/**
 * 业务对象类
 */
public class Mail {
    //收件人
    private  String receiver;
    //邮件名称
    private String advName;
    //称为
    private String application;
    //邮件内容
    private String context;
    //邮件的尾部
    private String tail;

    //构造函数
    public Mail(AdvTemplate advTemplate){
        this.advName = advTemplate.getAdvName();
        this.context = advTemplate.getAdvContext();
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getAdvName() {
        return advName;
    }

    public void setAdvName(String advName) {
        this.advName = advName;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getTail() {
        return tail;
    }

    public void setTail(String tail) {
        this.tail = tail;
    }
}
