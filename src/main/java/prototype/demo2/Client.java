package prototype.demo2;

import java.util.Random;

public class Client {
    //发送的数量
    private static int MAX_COUNT = 6;

    /**
     * 这个方法把对象复制一份,产生一个新的对象,和原有对象一样然后再修改细节的数据,如设置称谓设置收件人地址等;
     * 这种不通过new关键字来产生一个对象而是通过对象复制来实现的模式就叫做原型模式
     * @param args
     */
    public static void main(String[] args){
        //把模板定义出来
        Mail mail = new Mail(new AdvTemplate());
        mail.setTail("XXX版权所有");
        for (int i = 0; i < MAX_COUNT; i++) {
            //clone主题
            Mail cloneMail = mail.clone();
            //没封邮件的不同处
            cloneMail.setApplication(getRandString(5) + "先生(女士)");
            cloneMail.setReceiver(getRandString(5)+ "@" + getRandString(8) + ".com");
            sendMail(cloneMail);
        }
    }

    public static String getRandString(int maxLength){
        String str = "abcdefghijklmnopqrskuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuffer sb = new StringBuffer();
        Random random = new Random();
        for (int i = 0; i < maxLength; i++) {
               sb.append(str.charAt(random.nextInt(str.length())));
        }
        return sb.toString();
    }

    public static void sendMail(Mail mail){
        System.out.println("标题:" + mail.getAdvName());
        System.out.println("收件人:" + mail.getReceiver());
        System.out.println("....");
        System.out.println("发送成功");
        System.out.println("");
    }
}
