package prototype.demo5;

import java.io.Serializable;
import java.util.ArrayList;

public class Thing implements Serializable {
    //定义一个私有变量
    private ArrayList<String> arrayList = new ArrayList<String>();

    public void setValue(String value){
        this.arrayList.add(value);
    }

    public ArrayList<String> getValue(){
        return this.arrayList;
    }
}
