package prototype.demo5;

/**
 * 利用
 */
public class Client {

    public static void main(String[] args){
        Thing thing = new Thing();
        thing.setValue("11111");
        CloneClass cloneClass = new CloneClass();
        Thing cloneThing = cloneClass.clone(thing);
        cloneThing.setValue("22222");
        System.out.println(thing.getValue());
    }
}
