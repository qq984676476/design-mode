package prototype.demo3;

/**
 * 原型模式，浅clone
 * 会clone对象，对象的内部数组及引用对象等都不clone，还是只想源对象的元素地址
 */
public class Client {
    public static void main(String[] args){
        //产生一个对象
        Thing thing = new Thing();
        Thing cloneThing = thing.clone();
        thing.setValue("1111");
        cloneThing.setValue("2222");
        System.out.println(cloneThing.getValue());
    }
}
