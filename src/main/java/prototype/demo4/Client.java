package prototype.demo4;

/**
 * 原型模式的深clone
 * 使用原型模式时,应用的成员变量必须满足两个条件才不会被拷贝
 * 1:类的成员变量,而不是方法内的变量,
 * 2:必须是一个可变的引用对象,而不是一个原始类型或不可变对象
 */

/**
 * 要实现clone方法,类的成员变量上就不能增加final关键字
 */
public class Client {
    public static void main(String[] args){
        //产生一个对象
        Thing thing = new Thing();
        Thing cloneThing = thing.clone();
        thing.setValue("1111");
        cloneThing.setValue("2222");
        System.out.println(cloneThing.getValue());
    }
}
