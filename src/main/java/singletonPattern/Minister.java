package singletonPattern;

public class Minister {

    public static void main(String arg[]){
//        for(int i = 0; i < 3; i++){
//            Emperor emperor = Emperor.getInstance();
//            emperor.say();
//        }

        for(int i = 0; i < 10; i++){
            Emperor emperor = Emperor.getInstance();
            emperor.say(emperor);
        }
    }
}
