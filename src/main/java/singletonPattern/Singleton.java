package singletonPattern;

public class Singleton {

    private static Singleton singleton = null;

    private Singleton(){

    }

    public static Singleton getInstance(){
        if(singleton == null){
            singleton = new Singleton();
        }

        return singleton;
    }

    public static void out(){
        System.out.println("Singleton" + singleton);
    }
}
