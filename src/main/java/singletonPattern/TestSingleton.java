package singletonPattern;

/**
 * 单利模式
 */
public class TestSingleton {
    public static void main(String arg[]){

        for(int i = 0; i < 3; i++){
            Singleton singleton = Singleton.getInstance();
            singleton.out();
        }

    }
}
