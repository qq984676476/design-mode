package singletonPattern;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 限制实例化个数
 */

public class Emperor {
    private static int maxNumOfEmperor = 3;
    private static List<String> names = new ArrayList<String>();
    private static List<Emperor> emperors = new ArrayList<Emperor>();
    private static int countNumOfEmperor = 0;
    private static String name;

    static {
        for(int i = 0; i < maxNumOfEmperor; i++){
            emperors.add(new Emperor("皇帝" + i));
        }
    }

    private Emperor(){

    }

    private Emperor(String name){//防止new出这个对象
        this.name = name;
        names.add(name);
    }

    public static void say(Emperor emperor){
        System.out.println(emperor.getName() + emperor);
    }

    public static Emperor getInstance(){
        Random random = new Random();
        countNumOfEmperor =random.nextInt(maxNumOfEmperor);
        return emperors.get(countNumOfEmperor);
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        Emperor.name = name;
    }
}
