package strategy.demo2;

/**
 * 枚举类
 */
public enum Calculator {
    //加法运算
    ADD("+"){
        public int exec(int a, int b){
            return a + b;
        }
    },
    //减法运算
    SUB("-"){
        public int exec(int a, int b){
            return a + b;
        }
    };

    String value;

    //定义成员类型
    private Calculator (String _value) {
        this.value = _value;
    }

    //获得枚举成员

    public String getValue() {
        return this.value;
    }

    //生声明一个抽象函数
    public abstract int exec(int a, int b);
}
