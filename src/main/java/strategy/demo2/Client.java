package strategy.demo2;

import java.util.Arrays;

public class Client {
    public static void main(String[] args){

        args = new String[]{"2", "-", "5"};

        System.out.println("输入的参数为" + Arrays.toString(args));

        //参数1
        int a = Integer.parseInt(args[0]);

        //参数2
        String symbol = args[1];

        //参数3
        int b = Integer.parseInt(args[2]);



        System.out.println("运行结果为" + Calculator.ADD.exec(a, b));
    }
}
