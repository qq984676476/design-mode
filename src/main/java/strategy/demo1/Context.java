package strategy.demo1;

/**
 * 将每个算法都封装起来,封装策略方便使用
 */
public class Context {
    IStrategy iStrategy;
    public Context(IStrategy _iStrategy){
        this.iStrategy = _iStrategy;
    }

    public void operater(){
        this.iStrategy.operater();
    }
}
