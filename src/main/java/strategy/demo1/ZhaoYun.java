package strategy.demo1;

/**
 * 策略模式也叫作政策模式:定义一组算法,将每个算法都封装起来,并且使他们之间可以互换.
 */

public class ZhaoYun {
    public static void main(String[] args){
        Context context;
        //刚到吴国拆开第一个锦囊
        context = new Context(new BackDoor());
        context.operater();

        //拆第二个
        context = new Context(new GivenGreenLight());
        context.operater();

        //拆第三个
        context = new Context(new BlockEnemy());
        context.operater();
    }
}
