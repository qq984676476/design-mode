package adapter.demo2;

/**
 * 适配器模式拓展,同时融合多个接口,通过对象进行委托,
 * 通过对象进行适配的叫对象适配器,通过继承进行适配的叫做类适配器
 */
public class Client {
    public static void main(String[] args){
        IOuterUserBaseInfo baseInfo = new OuterBaseInfo();
        IOuterUserHomeInfo homeInfp = new OuterHomeInfo();
        IOuterUserOfficeInfo officeInfo = new OuterUserOfficeInfo();

        IUserInfo outerUserInfo= new OuterUserInfo(baseInfo, homeInfp, officeInfo);
        System.out.println(outerUserInfo.getJobPosition());
    }
}
