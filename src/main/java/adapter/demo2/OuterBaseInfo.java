package adapter.demo2;

import java.util.HashMap;
import java.util.Map;

public class OuterBaseInfo implements IOuterUserBaseInfo {
    public Map getUserBaseInfo() {
        Map<String, String> baseInfo = new HashMap<String, String>();
        baseInfo.put("userName", "姓名");
        baseInfo.put("mobileNumber", "员工电话");
        return baseInfo;
    }
}
