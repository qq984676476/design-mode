package adapter.demo2;

import java.util.HashMap;
import java.util.Map;

public class OuterUserOfficeInfo implements IOuterUserOfficeInfo {
    public Map getUserOfficeInfo() {
        Map<String, String> officeInfo = new HashMap<String, String>();
        officeInfo.put("jobPosition", "Boss...");
        officeInfo.put("officeTelNumber", "公司电话");
        return officeInfo;
    }
}
