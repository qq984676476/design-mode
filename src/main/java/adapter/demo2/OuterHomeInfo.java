package adapter.demo2;

import java.util.HashMap;
import java.util.Map;

public class OuterHomeInfo implements IOuterUserHomeInfo {
    public Map getUserHomeInfo() {
        Map<String, String> homeInfo = new HashMap<String, String>();
        homeInfo.put("homeTelNumber", "家庭电话");
        homeInfo.put("homeAddress", "家庭地址");
        return homeInfo;
    }
}
