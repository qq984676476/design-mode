package adapter.demo2;

import java.util.Map;

public class OuterUserInfo implements IUserInfo{
    private IOuterUserBaseInfo baseInfo = null;
    private IOuterUserHomeInfo homeInfo = null;
    private IOuterUserOfficeInfo officeInfo = null;

    private Map<String, String> baseInfoMap = null;
    private Map<String, String> homeInfoMap = null;
    private Map<String, String> officeInfoMap = null;

    public OuterUserInfo(IOuterUserBaseInfo _baseInfo, IOuterUserHomeInfo _homeInfo, IOuterUserOfficeInfo _officeInfo){
        this.baseInfo = _baseInfo;
        this.homeInfo = _homeInfo;
        this.officeInfo = _officeInfo;

        this.baseInfoMap = this.baseInfo.getUserBaseInfo();
        this.homeInfoMap = this.homeInfo.getUserHomeInfo();
        this.officeInfoMap = this.officeInfo.getUserOfficeInfo();
    }

    public String getUserName() {
        System.out.println(this.baseInfoMap.get("userName"));
        return this.baseInfoMap.get("userName");
    }

    public String getHomeAddress() {
        System.out.println(this.homeInfoMap.get("homeAddress"));
        return this.homeInfoMap.get("homeAddress");
    }

    public String getMobileNumber() {
        System.out.println(this.baseInfoMap.get("mobileNumber"));
        return this.baseInfoMap.get("mobileNumber");
    }

    public String getOfficeTelNumber() {
        System.out.println(this.officeInfoMap.get("officeTelNumber"));
        return this.officeInfoMap.get("officeTelNumber");
    }

    public String getJobPosition() {
        System.out.println(this.officeInfoMap.get("jobPosition"));
        return this.officeInfoMap.get("jobPosition");
    }

    public String getHomeTelNumber() {
        System.out.println(this.homeInfoMap.get("homeTelNumber"));
        return this.homeInfoMap.get("homeTelNumber");
    }
}
