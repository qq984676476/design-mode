package adapter.demo2;

public interface IUserInfo {

    String getUserName();
    String getHomeAddress();
    String getMobileNumber();
    String getOfficeTelNumber();
    String getJobPosition();
    String getHomeTelNumber();
}
