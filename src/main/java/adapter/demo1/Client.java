package adapter.demo1;

import javax.imageio.spi.IIOServiceProvider;

/**
 * 适配器模式
 */
public class Client {
    public static void main(String[] args){
        IUserInfo userInfo = new UserInfo();

        System.out.println(userInfo.getHomeTelNumber());

        IUserInfo outerUserInfo = new OuterUserInfo();

        System.out.println(outerUserInfo.getHomeTelNumber());


    }
}
