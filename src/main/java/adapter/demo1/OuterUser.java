package adapter.demo1;

import java.util.HashMap;
import java.util.Map;

public class OuterUser implements IOuterUser {
    public Map getUserBaseInfo() {
        Map map = new HashMap();
        map.put("userName", "outerUserName");
        map.put("mobileNumber", "outerUserMobileNumber");
        return map;
    }

    public Map getUserOfficeInfo() {
        Map map = new HashMap();
        map.put("jobPosition", "outerUserJobPosition");
        map.put("officeTelNumber", "outerUserOfficeTelNumber");
        return null;
    }

    public Map getUserHomeInfo() {
        Map map = new HashMap();
        map.put("homeTelNumber", "outerUserHomeTelNumber");
        map.put("homeAddress", "outerUserHomeAddress");
        return map;
    }
}
