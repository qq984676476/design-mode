package adapter.demo1;

import java.util.Map;

public interface IOuterUser {
    Map getUserBaseInfo();
    Map getUserOfficeInfo();
    Map getUserHomeInfo();
}
