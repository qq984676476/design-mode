package adapter.demo1;

public class UserInfo implements IUserInfo {
    public String getUserName() {
        return "getUserName";
    }

    public String getHomeAddress() {
        return "getHomeAddress";
    }

    public String getMobileNumber() {
        return "getMobileNumber";
    }

    public String getOfficeTelNumber() {
        return "getOfficeTelNumber";
    }

    public String getJobPosition() {
        return "getJobPosition";
    }

    public String getHomeTelNumber() {
        return "getHomeTelNumber";
    }
}
