package adapter.demo1;

import java.util.Map;

public class OuterUserInfo extends OuterUser implements IUserInfo {

    private Map baseInfo = super.getUserBaseInfo();
    private Map homeInfo = super.getUserHomeInfo();
    private Map officeInfo = super.getUserOfficeInfo();

    //姓名
    public String getUserName() {
        return (String) this.baseInfo.get("userName");
    }

    //家庭地址
    public String getHomeAddress() {
        return (String) this.homeInfo.get("homeAddress");
    }

    //手机号码
    public String getMobileNumber() {
        return (String) this.baseInfo.get("mobileNumber");
    }

    //办公电话
    public String getOfficeTelNumber() {
        return (String) this.officeInfo.get("officeTelNumber");
    }

    //职位信息
    public String getJobPosition() {
        return (String) this.officeInfo.get("outerUserJobPosition");
    }

    //家庭电话
    public String getHomeTelNumber() {
        return (String) this.homeInfo.get("homeTelNumber");
    }
}
