package command.demo2;

/**
 * 具体业务类直接出现在场景类里,
 * 每个命令完成单一的职责,而不是根据接受者不同完成不通的职责,在高层
 * 模块调用时就不用考虑接受者是谁
 */
public class Client {
    public static void main(String[] args){
        //声明调用者
        Invoker invoker = new Invoker();
        //定义发送给接受者的命令
        Command command = new ConcreteCommand2();
        //吧命令交给调用着去执行
        invoker.setCommand(command);
        invoker.action();
    }
}
