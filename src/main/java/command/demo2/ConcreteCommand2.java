package command.demo2;

public class ConcreteCommand2 extends Command {

    //默认的接收者
    public ConcreteCommand2(){
        super(new ConcreteReceiver2());
    }

    //设置新的 接收者
    public ConcreteCommand2(Receiver receiver){
        super(receiver);
    }

    //执行命令
    public void execute() {
        super.receiver.doSomething();
    }
}
