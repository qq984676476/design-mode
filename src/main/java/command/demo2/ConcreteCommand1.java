package command.demo2;

public class ConcreteCommand1 extends Command {

    //声明自己默认的接收者
    public ConcreteCommand1(){
        super(new ConcreteReceiver1());
    }

    //设置新的接受者
    public ConcreteCommand1(Receiver receiver){
        super(receiver);
    }

    //执行命令
    public void execute() {
        //处理业务逻辑
        super.receiver.doSomething();
    }
}
