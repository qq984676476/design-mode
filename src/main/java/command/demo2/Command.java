package command.demo2;

public abstract class Command {
    protected final Receiver receiver;

    //实现类必须定义一个接受者
    public Command(Receiver _receiver){
        this.receiver = _receiver;
    }

    //每个命令都必须有一个执行命令的方法
    public abstract void execute();
}
