package command.demo1;

//接头人
public class Invoker {
    //命令
    private Command command;

    //客户发出命令
    public void setCommand(Command _command){
        this.command = _command;
    }

    //执行命令
    public void action(){
        this.command.execute();
    }
}
