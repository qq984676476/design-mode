package command.demo1;

//删除页面的命令
public class DeletePageCommand extends Command{

    public void execute() {
        super.pageGroup.find();
        super.pageGroup.delete();
        super.pageGroup.plan();
    }
}
