package command.demo1;

public abstract class Command {
    protected RequirmentGroup requirmentGroup = new RequirmentGroup();
    protected PageGroup pageGroup = new PageGroup();
    protected CodeGroup codeGroup = new CodeGroup();

    public abstract void execute();
}
