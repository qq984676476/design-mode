package command.demo1;

/**
 *命令模式,核心为命令角色类,
 * 此示例场景类与具体业务类完全隔离
 *
 */

public class Client {
    public static void main(String[] args){
        //定义接头人
        Invoker invoker = new Invoker();

        //新增一个需求
        Command command = new AddRequirmentCommand();
        //接到命令
        invoker.setCommand(command);
        //执行命令
        invoker.action();
    }
}
