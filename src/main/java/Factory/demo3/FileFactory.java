package Factory.demo3;

public class FileFactory implements MakeFactory{

    public MakeFile createKlgFile() {
        MakeFile kleFile = new MakeKnowledgeFile();
        return kleFile;
    }

    public MakeFile createCatlFile() {
        MakeFile catlFile = new MakeCatlFile();
        return catlFile;
    }

    public MakeFile createTmpFile() {
        MakeFile tmpFile = new MakeTmpFile();
        return tmpFile;
    }
}
