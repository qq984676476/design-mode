package Factory.demo3;

public interface MakeFactory {
    MakeFile createKlgFile();
    MakeFile createCatlFile();
    MakeFile createTmpFile();

}
