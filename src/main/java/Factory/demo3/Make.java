package Factory.demo3;

import sun.applet.Main;

import java.util.HashMap;
import java.util.Map;

public class Make {
    public static void main(String[] args){
        MakeFactory makeFactory = new FileFactory();
        MakeFile klgFile = makeFactory.createKlgFile();
        Map context = new HashMap();
        context.put("ids", klgFile.getIds());
        context.put("param", klgFile.getParam());
        context.put("head", klgFile.setHead());
        context.put("content", klgFile.make());

        MakeFile catlFile = makeFactory.createCatlFile();
        context.put("ids", catlFile.getIds());
        context.put("param", catlFile.getParam());
        context.put("head", catlFile.setHead());
        context.put("content", catlFile.make());
        MakeFile tmpFile = makeFactory.createTmpFile();
        context.put("ids", tmpFile.getIds());
        context.put("param", tmpFile.getParam());
        context.put("head", tmpFile.setHead());
        context.put("content", tmpFile.make());
    }
}
