package Factory.demo3;

import java.util.List;
import java.util.Map;

public interface MakeFile {
    List getIds();
    Map getParam();
    Map make();
    Map setHead();
}
