package Factory.demo3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MakeCatlFile extends AbstractMakeCatlFile{
    public List getIds() {
        List list = new ArrayList();
        list.add("catl");
        return list;
    }

    public Map getParam() {
        Map map = new HashMap();
        map.put("param","catl");
        return map;
    }

    public Map make() {
        Map map = new HashMap();
        map.put("content", "catl");
        return map;
    }
}
