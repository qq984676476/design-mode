package Factory.demo3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MakeTmpFile extends AbstractMakeTmpFile {
    public List getIds() {
        List list = new ArrayList();
        list.add("tmp");
        return list;
    }

    public Map getParam() {
        Map map = new HashMap();
        map.put("param","tmp");
        return map;
    }

    public Map make() {
        Map map = new HashMap();
        map.put("content", "tmp");
        return map;
    }
}
