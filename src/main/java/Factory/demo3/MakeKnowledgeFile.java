package Factory.demo3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MakeKnowledgeFile extends AbstractMakeKnowledgeFile {
    public List getIds() {
        List list = new ArrayList();
        list.add("klg");
        return list;
    }

    public Map getParam() {
        Map map = new HashMap();
        map.put("param","klg");
        return map;
    }

    public Map make() {
        Map map = new HashMap();
        map.put("content", "klg");
        return map;
    }
}
