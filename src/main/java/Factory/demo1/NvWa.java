package Factory.demo1;

/**
 * 工厂模式
 */
public class NvWa {
    public static void main(String arg[]){
        AbstractHumanFactory factory = new HumanFactory();
        Human whiteHuman = factory.createHuman(WhiteHuman.class);
        whiteHuman.getColor();
        whiteHuman.talk();

        Human blackHuman = factory.createHuman(BlackHuman.class);
        blackHuman.getColor();
        blackHuman.talk();

        Human yellowHuman = factory.createHuman(YellowHuman.class);
        yellowHuman.getColor();
        yellowHuman.talk();

        Human brownHuman = factory.createHuman(BrownHuman.class);
        brownHuman.getColor();
        brownHuman.talk();
    }
}
