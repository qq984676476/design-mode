package Factory.demo1;

public class HumanFactory extends AbstractHumanFactory{
    public <T extends Human> T createHuman(Class<T> c) {
        Human human = null;
        try {
            human = (T)Class.forName(c.getName()).newInstance();
        } catch (Exception e){
            System.out.println("创造失败");
        }
        return (T)human;
    }
}
