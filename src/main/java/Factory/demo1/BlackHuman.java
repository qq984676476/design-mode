package Factory.demo1;

public class BlackHuman implements Human{
    public void getColor() {
        System.out.println("黑色");
    }

    public void talk() {
        System.out.println("黑话");
    }
}
