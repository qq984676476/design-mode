package Factory.demo1;

public class WhiteHuman implements Human{
    public void getColor() {
        System.out.println("白色");
    }

    public void talk() {
        System.out.println("白话");
    }
}
