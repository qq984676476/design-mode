package Factory.demo1;

public interface Human {
    void getColor();

    void talk();
}
