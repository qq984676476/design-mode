package Factory.demo2;

public interface HumanFactory {
    Human createYellowHuman();
    Human createWhiteowHuman();
    Human createBlackwHuman();
}
