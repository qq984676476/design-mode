package Factory.demo2;

public class NvWa {
    public static void main(String[] args){
        HumanFactory maleFactory = new MaleFactory();
        HumanFactory femaleFactory = new FemaleFactory();

        Human maleYellowHuman = maleFactory.createYellowHuman();
        maleYellowHuman.getColor();
        maleYellowHuman.getSex();
        maleYellowHuman.talk();

        Human maleWhiteHuman = maleFactory.createWhiteowHuman();
        maleWhiteHuman.getColor();
        maleWhiteHuman.getSex();
        maleWhiteHuman.talk();

        Human maleBlackHuman = maleFactory.createBlackwHuman();
        maleBlackHuman.getColor();
        maleBlackHuman.getSex();
        maleBlackHuman.talk();

        Human femaleYellowHuman = femaleFactory.createYellowHuman();
        femaleYellowHuman.getColor();
        femaleYellowHuman.getSex();
        femaleYellowHuman.talk();

        Human femaleWhiteHuman = femaleFactory.createWhiteowHuman();
        femaleWhiteHuman.getColor();
        femaleWhiteHuman.getSex();
        femaleWhiteHuman.talk();

        Human famaleBlackHuman = femaleFactory.createBlackwHuman();
        famaleBlackHuman.getColor();
        famaleBlackHuman.getSex();
        famaleBlackHuman.talk();

    }
}
