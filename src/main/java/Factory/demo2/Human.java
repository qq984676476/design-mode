package Factory.demo2;

public interface Human {

    void getColor();
    void talk();
    void getSex();
}
