package Factory.demo2;

public class MaleFactory implements HumanFactory {
    public Human createYellowHuman() {
        return new MaleYellowHuman();
    }

    public Human createWhiteowHuman() {
        return new MaleWhiteHuman();
    }

    public Human createBlackwHuman() {
        return new MaleBlackHuman();
    }
}
