package Factory.demo2;

public class FemaleFactory implements HumanFactory{
    public Human createYellowHuman() {
        return new FemaleYellowHuman();
    }

    public Human createWhiteowHuman() {
        return new FemaleWhiteHuman();
    }

    public Human createBlackwHuman() {
        return new FemaleBlackHuman();
    }
}
