package decorate.demo1;

/**
 * 实现成绩单
 */

public class FouthGradeSchoolReport extends SchoolReport {
    void report() {
        //成绩单的格式是
        System.out.println("尊敬的XXX家长:");
        System.out.println("语文:62, 数学:65, 体育95");
        System.out.println("家长签名:");
    }

    void sign(String name) {
        System.out.println("家长签名:" + name);
    }
}
