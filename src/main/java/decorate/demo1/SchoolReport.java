package decorate.demo1;

/**
 * 抽象一个成绩单
 */
public abstract class SchoolReport {
    abstract void report();
    abstract void sign(String name);
}
