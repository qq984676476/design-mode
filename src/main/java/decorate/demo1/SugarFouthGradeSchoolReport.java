package decorate.demo1;

/**
 * 美化成绩单
 */
public class SugarFouthGradeSchoolReport extends FouthGradeSchoolReport {
    //首先定义要美化的方法
    private void reportHighScore(){
        System.out.println("这次考试语文最高75, 数学最高78");
    }

    //排名
    private void reportSort(){
        System.out.println("排名38");
    }

    //由于汇报的内容发生变化所以重写汇报方法
    @Override
    public void report(){
        this.reportHighScore();
        super.report();
        this.reportSort();

    }
}
