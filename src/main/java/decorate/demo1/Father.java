package decorate.demo1;

/**
 * 装饰模式
 */
public class Father {
    public static void main(String[] args){

        SchoolReport sr = new SugarFouthGradeSchoolReport();
        //看成绩单
        sr.report();
        sr.sign("小明之父");
    }
}
