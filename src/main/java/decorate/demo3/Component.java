package decorate.demo3;

/**
 * 抽象构建
 */
public abstract class Component {
    //抽象方法
    public abstract void operation();
}
