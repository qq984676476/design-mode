package decorate.demo3;

/**
 * 装饰角色,通常是一个抽象类
 */
public abstract class Decorator extends Component{
    //抽象构建
    Component component;

    //通过构造函数将被修饰的对象传递进来
    public Decorator(Component _component){
        this.component = _component;
    }

    //委托给被修饰者执行
    @Override
    public void operation(){
        this.component.operation();
    }

}
