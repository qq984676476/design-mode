package decorate.demo3;

/**
 * 构建实现
 */
public class ConcreateComponent extends Component{
    //具体业务,被修饰的方法
    @Override
    public void operation() {
        System.out.println("业务代码");
    }
}
