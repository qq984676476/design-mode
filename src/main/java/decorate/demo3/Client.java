package decorate.demo3;

/**
 * 装饰模式:动态的给一个对象增加一些额外的自责,就增加功能来说,装饰模式相比增加子类更加灵活
 */
public class Client {
    public static void main(String[] args){
        Component component = new ConcreateComponent();

        //第一次装饰
        component = new CreateDecorator1(component);

        //第二次装饰
        component = new CreateDecorator2(component);

        //修饰后的结果
        component.operation();
    }
}
