package decorate.demo3;

/**
 * 进行修饰
 */
public class CreateDecorator2 extends Decorator {
    public CreateDecorator2(Component _component) {
        super(_component);
    }

    //定义自己的修饰方法
    private void method2(){
        System.out.println("method2修饰");
    }

    //重写父类的Operation方法
    @Override
    public void operation(){
        super.operation();
        this.method2();

    }


}
