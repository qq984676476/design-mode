package decorate.demo3;

/**
 * 进行修饰
 */
public class CreateDecorator1 extends Decorator{
    public CreateDecorator1(Component _component) {
        super(_component);
    }

    //定义自己的修饰方法
    private void method1(){
        System.out.println("method1修饰");
    }

    //重写父类的Operation方法
    @Override
    public void operation(){
        super.operation();
        this.method1();

    }
}
