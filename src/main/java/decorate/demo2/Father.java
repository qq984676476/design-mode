package decorate.demo2;

/**
 * 装饰模式:动态的给一个对象增加一些额外的自责,就增加功能来说,装饰模式相比增加子类更加灵活
 */
public class Father {
    public static void main(String[] args){
        /*
        成绩单
         */
        SchoolReport sr;

        /*
        本来的成绩单
         */
        sr = new FouthGradeSchoolReport();

        /*
        加上最高分的成绩单
         */
        sr = new HighScoreDecorator(sr);

        /*
        加上排名的成绩单
         */
        sr = new SortDecorator(sr);
        sr.report();
        sr.sign("小明之父");
    }
}
