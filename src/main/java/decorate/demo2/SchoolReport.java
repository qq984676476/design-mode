package decorate.demo2;

/**
 * 抽象的成绩单
 */
public abstract class SchoolReport {
    abstract void report();
    abstract void sign(String name);
}
