package decorate.demo2;

/**
 * 修饰排名情况
 */
public class SortDecorator extends Decorator {
    public SortDecorator(SchoolReport sr) {
        super(sr);
    }

    //排名
    public void reportSort(){
        System.out.println("排名38");
    }

    @Override
    public void report(){
        super.report();
        this.reportSort();
    }
}
