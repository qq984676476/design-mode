package decorate.demo2;

/**
 * 最高成绩修饰
 */
public class HighScoreDecorator extends Decorator {
    public HighScoreDecorator(SchoolReport sr) {
        super(sr);
    }

    public void reportHeighScore(){
        System.out.println("这次考试语文最高75, 数学最高78");
    }

    @Override
    public void report(){
        this.reportHeighScore();
        super.report();
    }
}
