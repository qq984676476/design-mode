package decorate.demo2;

/**
 * Decorator的作用就是让子类封装SchoolReport的子类
 */

public abstract class Decorator extends SchoolReport {
    //定义一个成绩单
    private SchoolReport sr;
    public Decorator(SchoolReport sr){
        this.sr = sr;
    }

    //成绩单
    public void report(){
        this.sr.report();
    }

    //签名
    public void sign(String name){
        this.sr.sign(name);
    }
}
