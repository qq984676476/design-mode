package observerPattern.demo1;

import java.util.ArrayList;

public class HanFeiZi implements IHanFeiZi, Observable{
    //定义一个容器,存放所有被观察者
    private ArrayList<Observer> observers = new ArrayList<Observer>();

    /**
     * 韩非子吃饭
     */
    public void haveBreakFast() {
        System.out.println("韩非子吃饭");
        this.notifyObservers("韩非子吃饭");
    }

    /**
     * 韩非子娱乐
     */
    public void haveFun() {
        System.out.println("韩非子娱乐");
        this.notifyObservers("韩非子娱乐");
    }

    /**
     * 增加观察者
     * @param observer
     */
    public void addObserver(Observer observer) {
        this.observers.add(observer);
    }

    /**
     * 删除观察者
     * @param observer
     */
    public void deleteObservable(Observer observer) {
        this.observers.remove(observer);
    }

    /**
     * 通知观察者
     * @param context
     */
    public void notifyObservers(String context) {
        for(Observer observer : observers){
            observer.update(context);
        }
    }
}
