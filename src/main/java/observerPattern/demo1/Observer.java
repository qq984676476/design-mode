package observerPattern.demo1;

/**
 * 观察者接口
 */
public interface Observer {
    void update(String context);
}
