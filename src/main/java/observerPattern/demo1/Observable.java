package observerPattern.demo1;

/**
 * 被观察者借口类
 */

public interface Observable {
    /*
     *增加一个观察者
     */
    void addObserver(Observer observer);

    /*
     *删除一个观察者
     */
    void deleteObservable(Observer observer);

    /*
     * 观察到变化通知观察者
     */
    void notifyObservers(String context);
}
