package observerPattern.demo1;

/**
 * 观察者模式:又叫订阅模式,定义对象间一对多的关系,使得每当一个对象改变状态,则
 * 所有依赖他的对象都会得到通知.
 * 被观察者接口类:必须实现的职责,他必须能够动态的添加或者删除观察者.
 * 观察者接口类:收到消息后即进行相关操作
 * 具体的被观察者:定义被观察者自己的业务逻辑,同时定义哪些事件进行通知
 * 具体的观察者:每个观察者在接收到消息后的处理反应是不同的,各个观察者有自己的处理逻辑
 *
 */
public class Client {
    public static void main(String[] args){
        //创创建持观察者
        Observer lisi = new LiSi();
        Observer liusi = new LiuSi();

        //创建被观察者
        HanFeiZi hanFeiZi = new HanFeiZi();
        hanFeiZi.addObserver(lisi);
        hanFeiZi.addObserver(liusi);

        hanFeiZi.haveBreakFast();
        hanFeiZi.haveFun();
        hanFeiZi.deleteObservable(liusi);
        hanFeiZi.haveBreakFast();
    }
}
