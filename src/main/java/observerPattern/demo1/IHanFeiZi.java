package observerPattern.demo1;

/**
 * 被观察者
 */
public interface IHanFeiZi {
    void haveBreakFast();
    void haveFun();
}
