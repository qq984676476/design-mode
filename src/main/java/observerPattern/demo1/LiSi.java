package observerPattern.demo1;

public class LiSi implements Observer {
    /**
     * 观察者触发方法
     * @param context
     */
    public void update(String context) {
        System.out.println("李斯:开始向老板报告");
        this.reportToQinShiHang(context);
    }

    private void reportToQinShiHang(String context){
        System.out.println("李斯:报告秦老板,韩非子" + context);
    }
}
