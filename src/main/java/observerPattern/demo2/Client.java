package observerPattern.demo2;
import java.util.Observer;

/**
 * 观察者模式扩展: 利用JDK定义的Observable(被观察者) 及Observer(观察者).
 * 观察者和被观察者之间的消息传递:观察者的update方法接受两个参数,一个是被观察者,
 * 一个是DTO(Date Transfer Obiect,数据传输对象),DTO一般是一个纯洁的javaBean,由被观察者生成
 * ,由观察者消费
 */
public class Client {
    public static void main(String[] args){
        //创建观察者
        Observer liSi = new LiSi();
        //创建被观察者
        HanFeiZi hanFeiZi = new HanFeiZi();

        //添加观察者
        hanFeiZi.addObserver(liSi);

        hanFeiZi.haveBreakFast();

        hanFeiZi.haveFun();
    }
}
