package observerPattern.demo2;

import java.util.Observable;
import java.util.Observer;

public class LiSi implements Observer {
    public void update(Observable o, Object arg) {
        System.out.println("韩非子监视到:" + arg.toString());
        this.reportToQinShiHang(arg.toString());
    }

    private void reportToQinShiHang(String context){
        System.out.println("韩非子向秦老板报告:" + context);
    }
}
