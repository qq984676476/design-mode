package observerPattern.demo2;

public interface IHanFeiZi {
    /**
     * 吃饭
     */
    void haveBreakFast();

    /**
     * 娱乐
     */
    void haveFun();
}
