package observerPattern.demo2;

import java.util.Observable;

public class HanFeiZi extends Observable implements IHanFeiZi {
    public void haveBreakFast() {
        System.out.println("韩非子吃饭");
        super.setChanged();
        super.notifyObservers("韩非子吃饭");
    }

    public void haveFun() {
        System.out.println("韩非子娱乐");
        super.setChanged();
        super.notifyObservers("韩非子娱乐");
    }
}
