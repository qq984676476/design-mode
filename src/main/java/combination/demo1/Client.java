package combination.demo1;

import javax.swing.*;
import java.beans.beancontext.BeanContext;
import java.util.ArrayList;

/**
 * 组合模式
 */
public class Client {
    public static void main(String[] args){
        //
        Branch ceo = compostieCorpTree();
        String info = getTreeInfo(ceo);
        System.out.println(info);
    }

    /**
     * 组装一个树结构
     * @return
     */
    public static Branch compostieCorpTree(){
        //总经理
        Branch root = new Branch("王大麻子", "总经理", 1000000);

        //三个部门经理
        Branch developDep = new Branch("研发经理", "研发经理", 100000);
        Branch salesDep = new Branch("销售经理", "销售经理", 100000);
        Branch financeDep = new Branch("财务经理", "财务经理", 100000);

        //两个组长
        Branch firstDevGroup = new Branch("开发一组", "开发一组",  10000);
        Branch secondDep = new Branch("开发二组", "开发二组", 10000);


        //小兵
        Branch A = new Branch("A", "开发", 5000);
        Branch B = new Branch("B", "开发", 5000);
        Branch C = new Branch("C", "开发", 5000);
        Branch H = new Branch("H", "开发副经理", 5000);
        Branch D = new Branch("D", "秘书", 5000);
        Branch E = new Branch("E", "销售", 5000);
        Branch F = new Branch("F", "销售", 5000);
        Branch G = new Branch("G", "财务", 5000);

        //总经理
        root.addSubordiante(developDep);
        root.addSubordiante(salesDep);
        root.addSubordiante(financeDep);
        root.addSubordiante(D);

        //部门经理
        developDep.addSubordiante(H);
        developDep.addSubordiante(firstDevGroup);
        developDep.addSubordiante(secondDep);

        //开发组长
        firstDevGroup.addSubordiante(A);
        firstDevGroup.addSubordiante(B);
        secondDep.addSubordiante(C);

        //销售部
        salesDep.addSubordiante(E);
        salesDep.addSubordiante(F);

        //财务部
        financeDep.addSubordiante(G);

        return root;
    }

    public static String getTreeInfo(Branch branch){
        ArrayList<ICorp> subordianteList = branch.getSubordianteInfo();

        String info = branch.getInfo() + "\n";
        for(ICorp corp : subordianteList){
            if(corp instanceof Leaf){
                info += corp.getInfo() + "\n";
            }else{
                info += corp.getInfo() + "\n" + getTreeInfo((Branch) corp);
            }

        }
        return info;
    }
}
