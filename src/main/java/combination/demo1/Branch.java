package combination.demo1;

import java.util.ArrayList;

public class Branch implements IBranch{

    //名字
    private String name;

    //职位
    private String position;

    //薪水
    private int salary;

    //子节点
    private ArrayList<ICorp> subordianteList = new ArrayList<ICorp>();

    public Branch(String name, String position, int salary) {
        this.name = name;
        this.position = position;
        this.salary = salary;
    }

    /**
     * 增加节点
     * @param corp
     */
    public void addSubordiante(ICorp corp) {
        this.subordianteList.add(corp);
    }

    /**
     * 增加叶子节点或者非叶子节点
     * @return
     */
    public ArrayList getSubordianteInfo() {
        return this.subordianteList;
    }



    public String getInfo() {
        String info = "";

        info += "姓名: " + this.name;
        info += "职位: " + this.position;
        info += "薪水: " + this.salary;

        return info;

    }
}
