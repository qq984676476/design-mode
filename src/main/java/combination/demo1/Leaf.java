package combination.demo1;

public class Leaf implements ILeaf {
    //名字
    private String name = "";

    //职位
    private String position = "";

    //薪水
    private int salary;

    public Leaf(String name, String position, int salary) {
        this.name = name;
        this.position = position;
        this.salary = salary;
    }

    public String getInfo() {
        String info = "";

        info += "姓名: " + this.name + "  ";
        info += "   职位: " + this.position + "  ";
        info += "   薪水: " + this.salary + "  ";

        return info;

    }
}
