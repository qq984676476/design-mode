package combination.demo1;

import java.util.ArrayList;

public interface IBranch extends ICorp{

    /**
     * 增加节点
     * @param corp
     */
    void addSubordiante(ICorp corp);

    /**
     * 遍历节点
     * @return
     */
    ArrayList getSubordianteInfo();
}
