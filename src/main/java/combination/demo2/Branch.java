package combination.demo2;

import java.util.ArrayList;

public class Branch extends Corp {

    //子节点
    private ArrayList<Corp> subordianteList = new ArrayList<Corp>();

    public Branch(String name, String position, int salary) {
        super(name, position, salary);
    }

    /**
     * 增加节点
     * @param corp
     */
    public void addSubordiante(Corp corp) {
        this.subordianteList.add(corp);
    }

    /**
     * 增加叶子节点或者非叶子节点
     * @return
     */
    public ArrayList getSubordianteInfo() {
        return this.subordianteList;
    }


}
