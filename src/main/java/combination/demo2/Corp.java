package combination.demo2;

import combination.demo1.ICorp;

import java.util.ArrayList;

public abstract class Corp {
    //名字
    private String name;

    //职位
    private String position;

    //薪水
    private int salary;


    public Corp(String name, String position, int salary) {
        this.name = name;
        this.position = position;
        this.salary = salary;
    }

    public String getInfo() {
        String info = "";

        info += "姓名: " + this.name;
        info += "职位: " + this.position;
        info += "薪水: " + this.salary;

        return info;

    }
}
