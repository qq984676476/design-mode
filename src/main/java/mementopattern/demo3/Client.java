package mementopattern.demo3;

public class Client {
    public static void main(String[] args){
        //声明发起人
        Originator originator = new Originator();
        //声明备忘录管理员
        Careaker careaker = new Careaker();
        //初始化
        originator.setState1("中国");
        originator.setState2("强盛");
        originator.setState3("繁荣");

        System.out.println(originator.toString());
        //创建备忘录
        careaker.setMemento(originator.createMemento());

        //修改状态值
        originator.setState1("少年");
        originator.setState2("爱学");
        originator.setState3("国旺");
        System.out.println(originator.toString());

        //回退状态
        originator.restoreMemento(careaker.getMemento());
        System.out.println(originator.toString());

    }
}
