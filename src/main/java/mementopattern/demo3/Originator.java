package mementopattern.demo3;

public class Originator {
    String state1;
    String state2;
    String state3;

    public String getState2() {
        return state2;
    }

    public void setState2(String state2) {
        this.state2 = state2;
    }

    public String getState3() {
        return state3;
    }

    public void setState3(String state3) {
        this.state3 = state3;
    }

    public String getState1() {

        return state1;
    }

    public void setState1(String state1) {
        this.state1 = state1;
    }

    //创建一个备忘录
    public Memento createMemento(){
        return new Memento(BeanUtil.backupProp(this));
    }

    //回退状态
    public void restoreMemento(Memento _memento){
        BeanUtil.restoreProp(this, _memento.getStateMap());
    }

    @Override
    public String toString() {
        return "Originator{" +
                "state1='" + state1 + '\'' +
                ", state2='" + state2 + '\'' +
                ", state3='" + state3 + '\'' +
                '}';
    }
}
