package mementopattern.demo4;

import java.util.HashMap;

/**
 * 备忘录管理员
 */
public class Careaker {
    private Memento memento;

    //盛放状态的容器
    private HashMap<String, Memento> mentoMap = new HashMap<String, Memento>();

    public Memento getMemento(String index) {
        return mentoMap.get(index);
    }

    public void setMemento(String index, Memento memento) {
        this.mentoMap.put(index, memento);
    }
}
