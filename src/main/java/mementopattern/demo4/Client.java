package mementopattern.demo4;

/**
 * 备忘录模式:检查点
 */
public class Client {
    public static void main(String[] args){
        //声明发起人
        Originator originator = new Originator();
        //声明备忘录管理员
        Careaker careaker = new Careaker();
        careaker.setMemento("001", originator.createMemento());
        careaker.setMemento("002", originator.createMemento());
        //初始化1
        originator.setState1("中国");
        originator.setState2("强盛");
        originator.setState3("繁荣");

        System.out.println(originator.toString());
        //创建备忘录
        careaker.setMemento("002",originator.createMemento());

        //初始化2
        originator.setState1("中国1");
        originator.setState2("强盛2");
        originator.setState3("繁荣3");

        System.out.println(originator.toString());
        //创建备忘录
        careaker.setMemento("001",originator.createMemento());

        //修改状态值
        originator.setState1("少年");
        originator.setState2("爱学");
        originator.setState3("国旺");
        System.out.println(originator.toString());

        //回退状态
        originator.restoreMemento(careaker.getMemento("001"));
        System.out.println(originator.toString());

    }
}
