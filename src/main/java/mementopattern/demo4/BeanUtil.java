package mementopattern.demo4;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.HashMap;

public class BeanUtil {

    /**
     * 把发起人的所有状态转换到HashMap中
     * @param bean
     * @return
     */
    public static HashMap<String, Object> backupProp(Object bean){
        HashMap<String, Object> result = new HashMap<String, Object>();
        try {
            //获得bean描述
            BeanInfo beanInfo = Introspector.getBeanInfo(bean.getClass());
            //获得属性描述
            PropertyDescriptor[] descriptors = beanInfo.getPropertyDescriptors();
            //遍历所有属性
            for(PropertyDescriptor descriptor : descriptors){
                //读取属性名称
                String fileName = descriptor.getName();
                //读取属性的方法
                Method getter = descriptor.getReadMethod();
                //读取属性值
                Object fileVallue = getter.invoke(bean, new Object[]{});
                // equalsIgnoreCase忽略大小写比较,equals不忽略大小写
                if(!fileName.equalsIgnoreCase("calss")){
                    result.put(fileName, fileVallue);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 回退状态
     * @param propMap
     */
    public static void restoreProp(Object objBean, HashMap<String, Object> propMap){
        try {
            //获得bean描述
            BeanInfo beanInfo = Introspector.getBeanInfo(objBean.getClass());
            //获取bean所有属性
            PropertyDescriptor[] descriptors = beanInfo.getPropertyDescriptors();
            for(PropertyDescriptor descriptor : descriptors){
                //读取属性名称
                String fileName = descriptor.getName();
                //如果有这个属性
                if(propMap.containsKey(fileName) && !fileName.equalsIgnoreCase("class")){
                    Method setter = descriptor.getWriteMethod();
                    setter.invoke(objBean, new Object[]{propMap.get(fileName)});
                }
            }

        } catch (Exception e) {
            System.out.println("shit");
            e.printStackTrace();
        }

    }
}
