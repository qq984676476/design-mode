package mementopattern.demo2;

/**
 * 备忘录模式
 */
public class Client {
    public static void main(String[] args){

        // 声明主角
        Boy boy = new Boy();
        // 声明备忘录管理者
        Caretaker caretaker = new Caretaker();

        //初始化当前状态
        boy.setState("心情很好");
        System.out.println(boy.getState());

        //创建备忘录,记录状态
        caretaker.setMemento(caretaker.getMemento());

        //改变状态
        boy.changeState();
        System.out.println(boy.getState());

        //恢复状态
        boy.restoreMemento(caretaker.getMemento());
        System.out.println(boy.getState());

    }
}
