package mementopattern.demo2;

/**
 * 备忘录类
 */
public class Memento {

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Memento(String state) {
        this.state = state;
    }

    private String state = "";
}
