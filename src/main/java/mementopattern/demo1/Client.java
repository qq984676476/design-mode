package mementopattern.demo1;

/**
 * 备忘录模式
 */
public class Client {
    public static void main(String[] args){

        /**
         * 生命主角
         *
         */
        Boy boy = new Boy();
        boy.setState("心情很好");
        System.out.println(boy.getState());

        //创建备忘录,记录状态
        Memento me = boy.createMemento();

        //改变状态
        boy.changeState();
        System.out.println(boy.getState());

        //恢复状态
        boy.restoreMemento(me);
        System.out.println(boy.getState());

    }
}
