package mementopattern.demo1;

public class Boy {

    String state = "";

    /**
     * 留一个备份
     * @return
     */
    public Memento createMemento(){
        return new Memento(this.state);
    }

    /**
     * 恢复备份
     * @param memento
     */
    public void restoreMemento(Memento memento){
        this.state = memento.getState();
    }

    public void changeState(){
        this.state = "心情不好";

    }


    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
