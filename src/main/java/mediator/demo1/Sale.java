package mediator.demo1;

import java.util.Random;

public class Sale {
    //销售IBM电脑
    public void sellIBMComputer(int count){
        //仓库
        Stock stock = new Stock();

        //采购
        Purchase purchase = new Purchase();
        if(stock.getCOMPUTER_NUMBER() < count){//库存不够销售
            purchase.buyIBMComputer(count);
        }

        System.out.println("销售IBM" + count + "台");
        stock.decrease(count);
    }

    //反馈销售情况,0--100之间变化,0代表没人买,100代表非常畅销
    public int getSaleStatus(){
        Random random = new Random(System.currentTimeMillis());
        int status = random.nextInt(100);
        System.out.println("电脑的销售情况是" + status);
        return status;
    }

    //折价处理
    public void offSale(){
        //库房有多少卖多少
        Stock stock = new Stock();
        System.out.println("折价销售" + stock.getCOMPUTER_NUMBER() + "台");
    }
}
