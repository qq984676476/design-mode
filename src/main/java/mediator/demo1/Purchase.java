package mediator.demo1;

public class Purchase {

    //采购电脑
    public void buyIBMComputer(int number){
        //访问库存
        Stock stock = new Stock();
        //访问销售
        Sale sale = new Sale();
        //电脑的销售情况
        int saleStatus = sale.getSaleStatus();
        if(saleStatus > 80){ //销售情况良好
            System.out.println("采购IBM" + number + "台");
            stock.increase(number);
        }else {//销售情况不好
            int buyNumber = number/2;//折半采购
            System.out.println("采购IBM" + number + "台");
        }
    }

    //不再采购
    public void refuseBuyIBM(){
        System.out.println("不再采购");
    }
}
