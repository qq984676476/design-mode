package mediator.demo1;

public class Stock {
    //刚开始有100台
    private int COMPUTER_NUMBER = 100;

    //库存增加
    public void increase(int number){
        COMPUTER_NUMBER = COMPUTER_NUMBER + number;
        System.out.println("现在的库存数量为" + "台");
    }

    //库存降低
    public void decrease(int number){
        COMPUTER_NUMBER = COMPUTER_NUMBER - number;
        System.out.println("现在的库存数量为" + "台");
    }

    //获得库存数量
    public int getCOMPUTER_NUMBER(){
        return COMPUTER_NUMBER;
    }

    //库存压力大了就通知采购人员停止采购,销售人员尽快销售
    public void clearStock(){
        Purchase purchase = new Purchase();
        Sale sale = new Sale();
        System.out.println("清理存货数量为:" + COMPUTER_NUMBER);

        //要求折价销售
        sale.offSale();

        //要求采购人员不要采购
        purchase.refuseBuyIBM();
    }
}
