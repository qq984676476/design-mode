package mediator.demo1;

/**
 * 未使用中介者模式的,库存,销售,采购管理
 */
public class Client {
    public static void main(String[] args){
        //开始采购电脑
        Purchase purchase = new Purchase();
        purchase.buyIBMComputer(100);

        //开始销售电脑
        Sale sale = new Sale();
        sale.sellIBMComputer(1);

        //库房管理人员管理库存
        Stock stock = new Stock();
        stock.clearStock();
    }
}
