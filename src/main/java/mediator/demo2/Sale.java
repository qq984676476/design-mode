package mediator.demo2;

import java.util.Random;

public class Sale extends AbstractColleague{

    public Sale(AbstractMediator _abstractMediator) {
        super(_abstractMediator);
    }

    //销售IBM电脑
    public void sellIBMComputer(int count){
        super.abstractMediator.execute("sale.sell", count);
        System.out.println("销售电脑" + count + "台");
    }
    //反馈销售情况,0--100之间变化,0代表没人买,100代表非常畅销
    public int getSaleStatus(){
        Random random = new Random(System.currentTimeMillis());
        int status = random.nextInt(100);
        System.out.println("电脑的销售情况是" + status);
        return status;
    }

    //折价处理
    public void offSale(){
        super.abstractMediator.execute("sale.sell");
    }

}
