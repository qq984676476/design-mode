package mediator.demo2;

//中介者抽象类
public abstract class AbstractMediator {
    //采购
    protected Purchase purchase;
    //销售
    protected Sale sale;
    //厂库
    protected Stock stock;

    public AbstractMediator() {
        purchase = new Purchase(this);
        sale = new Sale(this);
        stock = new Stock(this);
    }

    //中介者最重要的方法叫做事件方法
    public abstract void execute(String str, Object...ibj);
}
