package mediator.demo2;

//中介者实现类
public class Mediator extends AbstractMediator{
    //中介者模式的核心:中介方法

    public void execute(String str, Object... objs) {
        if(str.equals("purchase.buy")){//采购电脑
            this.buyIBMComputer((Integer)objs[0]);
        }else if(str.equals("sale.cell")){//销售电脑
            this.sellIBMComputer((Integer)objs[0]);
        }else if(str.equals("sale.offsell")){//折价销售
            this.offSale();
        }else if(str.equals("stock.clear")){
            this.clearStock();
        }
    }

    /**
     * 将需要与外界关联的方法提取到中介者方法中
     */
    //采购电脑
    private void buyIBMComputer(int number){
        //电脑的销售情况
        int saleStatus = super.sale.getSaleStatus();
        if(saleStatus > 80){ //销售情况良好
            System.out.println("采购IBM" + number + "台");
            super.stock.increase(number);
        }else {//销售情况不好
            int buyNumber = number/2;//折半采购
            System.out.println("采购IBM" + buyNumber + "台");
        }
    }

    //销售IBM电脑
    private void sellIBMComputer(int count){

        if(super.stock.getCOMPUTER_NUMBER() < count){//库存不够销售
            super.purchase.buyIBMComputer(count);
        }

        System.out.println("销售IBM" + count + "台");
        super.stock.decrease(count);
    }

    //折价处理
    public void offSale(){
        //库房有多少卖多少
        System.out.println("折价销售" + stock.getCOMPUTER_NUMBER() + "台");
    }

    //库存压力大了就通知采购人员停止采购,销售人员尽快销售
    public void clearStock() {
        //清仓
        super.sale.offSale();

        //停止采购
        super.purchase.refuseBuyIBM();
    }
}
