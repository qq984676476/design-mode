package mediator.demo2;

public abstract class AbstractColleague {
    protected AbstractMediator abstractMediator;

    public AbstractColleague(AbstractMediator _abstractMediator){
        this.abstractMediator = _abstractMediator;
    }
}
