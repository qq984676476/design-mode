package mediator.demo2;

/**
 * 使用中介者模式的,库存,销售,采购管理
 */
public class Client {
    public static void main(String[] args){
        AbstractMediator mediator = new Mediator();
        //采购电脑
        Purchase purchase = new Purchase(mediator);
        purchase.buyIBMComputer(100);

        //销售电脑
        Sale sale = new Sale(mediator);
        sale.sellIBMComputer(1);

        //管理库存
        Stock stock = new Stock(mediator);
        stock.clearStock();
    }
}
