package mediator.demo2;

public class Purchase extends AbstractColleague{

    public Purchase(AbstractMediator _abstractMediator) {
        super(_abstractMediator);
    }
    //采购电脑
    public void buyIBMComputer(int number){
        super.abstractMediator.execute("purchase.buy", number);
    }
    //不再采购
    public void refuseBuyIBM(){
        System.out.println("不再采购");
    }
}
