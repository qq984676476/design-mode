package agentModule.demo1;

/**
 * 代理模式
 */
public class Client {
    public static void main(String[] args){
        GamePlayer game = new GamePlayer("111");
        game.loggin();
        game.killBoss();
        game.upGrade();
    }
}
