package agentModule.demo2;

public class Client {
    public static void main(String[] args){
        GamePlayer game = new GamePlayer("111");
        GamePlayerProxy playerProxy = new GamePlayerProxy(game);//playerProxy
        playerProxy.loggin();
        playerProxy.killBoss();
        playerProxy.upGrade();
    }
}
