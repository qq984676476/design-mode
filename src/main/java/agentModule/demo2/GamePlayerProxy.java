package agentModule.demo2;

public class GamePlayerProxy implements IGamePlayer {
    private IGamePlayer player;
    public void loggin() {
        this.player.loggin();
    }

    public void killBoss() {
        this.player.killBoss();
    }

    public void upGrade() {
        this.player.upGrade();
    }

    public GamePlayerProxy(IGamePlayer gamePlayer){
        this.player = gamePlayer;
    }
}
