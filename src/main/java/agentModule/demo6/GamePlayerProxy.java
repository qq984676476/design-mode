package agentModule.demo6;

public class GamePlayerProxy implements IGamePlayer, IProxy {
    private IGamePlayer player;
    public void loggin() {
        this.player.loggin();
    }

    public void killBoss() {
        this.player.killBoss();
    }

    public void upGrade() {
        this.player.upGrade();
        this.count();
    }

    public IGamePlayer getgameProxy() {
        return this;
    }

    public GamePlayerProxy(IGamePlayer _player){
        this.player = _player;
    }

    /**
     * 代理类不仅仅是可以有自己的方法,通常的情况下代理的职责并不单一,
     * 它可以组合其他的真是角色,也可以实现自己的职责.
     * 代理类可以为真实角色预处理消息,过滤消息,消息转发,事后处理等功能。
     */
    public void count() {
        System.out.println("升级工花150");
    }
}
