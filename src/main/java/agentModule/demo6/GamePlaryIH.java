package agentModule.demo6;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 动态代理
 */
public class GamePlaryIH implements InvocationHandler{
    Class c = null;//被代理者
    Object o = null;//被代理的实例

    public GamePlaryIH(Object _o){
        this.o = _o;
    }

    //JDK动态代理接口
    //invoke必须实现,完成对现实方法的调用
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result = method.invoke(this.o, args);
        if(method.getName().equalsIgnoreCase("loggin")){
            System.out.println("现在登录");
        }
        return result;
    }
}
