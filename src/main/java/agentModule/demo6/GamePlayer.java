package agentModule.demo6;

public class GamePlayer implements IGamePlayer {
    private String name;
    private IGamePlayer proxy;
    public void loggin() {
        System.out.println(this.name + "登录");
    }

    public void killBoss() {
        System.out.println(this.name + "打怪");
    }

    public void upGrade() {
      System.out.println(this.name + "升级");
    }

    public  GamePlayer(String name){
        this.name = name;
    }
}
