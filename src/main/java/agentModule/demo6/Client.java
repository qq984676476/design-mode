package agentModule.demo6;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class Client {
    public static void main(String[] args){
//        GamePlayer player = new GamePlayer("111");//不使用代理
//        player.loggin();
//        player.killBoss();
//        player.upGrade();
//
//        GamePlayerProxy playerProxy = new GamePlayerProxy(player);//不适用指定代理
//        playerProxy.loggin();
//        playerProxy.killBoss();
//        playerProxy.upGrade();
//
//
//        IGamePlayer player1 = new GamePlayer("111");//使用指定代理
//        IGamePlayer playerProxy1 = player1.getgameProxy();
//        playerProxy1.loggin();
//        playerProxy1.killBoss();
//        playerProxy1.upGrade();

        //创建玩家
        IGamePlayer gamePlayer = new GamePlayer("111");
        //定义handler
        InvocationHandler handler = new GamePlaryIH(gamePlayer);
        //获得类的ClassLoad
        ClassLoader cl = gamePlayer.getClass().getClassLoader();
        //产生一个代理
        IGamePlayer procy = (IGamePlayer) Proxy.newProxyInstance(cl, new Class[]{IGamePlayer.class}, handler);
        procy.loggin();
        procy.killBoss();
        procy.upGrade();
    }
}
