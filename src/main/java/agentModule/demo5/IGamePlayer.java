package agentModule.demo5;

public interface IGamePlayer {
    void loggin();
    void killBoss();
    void upGrade();
    IGamePlayer getgameProxy();
}
