package agentModule.demo5;

public class GamePlayer implements IGamePlayer {
    private String name;
    private IGamePlayer proxy;
    public void loggin() {
        if(this.isProxy()){
            System.out.println(this.name + "登录");
        }else{
            System.out.println("请使用指定代理");
        }
    }

    public void killBoss() {
        if(this.isProxy()){
            System.out.println(this.name + "打怪");
        }else{
            System.out.println("请使用指定代理");
        }
    }

    public void upGrade() {
        if(this.isProxy()){
            System.out.println(this.name + "升级");
        }else{
            System.out.println("请使用指定代理");
        }
    }

    public IGamePlayer getgameProxy() {
        this.proxy = new GamePlayerProxy(this);
        return  this.proxy;
    }

    public  GamePlayer(String name){
        this.name = name;
    }

    private Boolean isProxy(){
        if(this.proxy == null){
            return false;
        }else {
            return true;
        }
    }
}
