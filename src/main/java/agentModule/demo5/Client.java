package agentModule.demo5;

public class Client {
    public static void main(String[] args){
        GamePlayer player = new GamePlayer("111");//不使用代理
        player.loggin();
        player.killBoss();
        player.upGrade();

        GamePlayerProxy playerProxy = new GamePlayerProxy(player);//不适用指定代理
        playerProxy.loggin();
        playerProxy.killBoss();
        playerProxy.upGrade();


        IGamePlayer player1 = new GamePlayer("111");//使用指定代理
        IGamePlayer playerProxy1 = player1.getgameProxy();
        playerProxy1.loggin();
        playerProxy1.killBoss();
        playerProxy1.upGrade();
    }
}
