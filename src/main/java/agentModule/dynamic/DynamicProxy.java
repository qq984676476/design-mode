package agentModule.dynamic;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class DynamicProxy<T> {
    public static <T>T newProxyInstance(ClassLoader loader, Class<?>[] Interface, InvocationHandler h) {
        //寻找jionPoint链接点,AOP框架使用元素定义
        if(true){
            //执行一个前置通知
            (new BeforAdvice()).exec();
        }

        //执行目标并返回结果
        return (T)Proxy.newProxyInstance(loader, Interface, h);
    }
}
