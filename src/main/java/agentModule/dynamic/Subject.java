package agentModule.dynamic;

/**
 * 抽象主体
 */
public interface Subject {
    //业务操作,可以有多个逻辑方法
    void doSomething(String str);
}
