package agentModule.dynamic;

/**
 * 使用封装的SubjectDynamiceProxy
 */
public class Client1 {
    public static void main(String[] args){
        //定义一个主题
        Subject subject = new RealSubject();
        Subject proxy = SubjectDynamiceProxy.newProxyInstance(subject);
        proxy.doSomething("222");
    }

}
