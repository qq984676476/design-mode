package agentModule.dynamic;

import java.lang.reflect.InvocationHandler;

public class SubjectDynamiceProxy extends DynamicProxy {
    public static <T>T newProxyInstance(Subject subject){
        //获得ClassLoad
        ClassLoader classLoader = subject.getClass().getClassLoader();

        //获得接口数组
        Class<?>[] classes = subject.getClass().getInterfaces();

        //获得handler
        InvocationHandler invocationHandler = new MyInvocationHandler(subject);
        return newProxyInstance(classLoader, classes, invocationHandler);
    }
}
