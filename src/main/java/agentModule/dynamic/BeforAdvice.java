package agentModule.dynamic;

public class BeforAdvice implements Advice {
    public void exec() {
        System.out.println("前置通知已被执行.");
    }
}
