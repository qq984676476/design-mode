package agentModule.dynamic;

import java.lang.reflect.InvocationHandler;

/**
 * 未封装SubjectDynamiceProxy
 */
public class Client {
    public static void main(String[] args){

        //定义一个主题
        Subject subject = new RealSubject();

        //定义一个handler
        InvocationHandler invocationHandler = new MyInvocationHandler(subject);

        //定义主题的代理
        /**
         * subject.getClass().getInterfaces()
         * 获取subject类的所有接口,获取到的全部是空方法
         *DynamicProxy为通用类,不具有任何业务意义
         */

        Subject prox = DynamicProxy.newProxyInstance(
                subject.getClass().getClassLoader(),
                subject.getClass().getInterfaces(),
                invocationHandler);
        prox.doSomething("11111111");
    }
}
