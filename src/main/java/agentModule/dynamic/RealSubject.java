package agentModule.dynamic;

/**
 * 真实主题
 */
public class RealSubject implements Subject {

    //业务操作
    public void doSomething(String str) {
        System.out.println("这里写具体逻辑" + str);
    }
}
