package agentModule.dynamic;

public interface Advice {
    //通知只有一个方法,执行即可
    void exec();
}
