package agentModule.demo4;

public class GamePlayerProxy implements IGamePlayer {
    private IGamePlayer player;
    public void loggin() {
        this.player.loggin();
    }

    public void killBoss() {
        this.player.killBoss();
    }

    public void upGrade() {
        this.player.upGrade();
    }

    public IGamePlayer getgameProxy() {
        return this;
    }

    public GamePlayerProxy(IGamePlayer _player){
        this.player = _player;
    }
}
