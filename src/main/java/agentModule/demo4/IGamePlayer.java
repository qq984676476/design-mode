package agentModule.demo4;

public interface IGamePlayer {
    void loggin();
    void killBoss();
    void upGrade();
    IGamePlayer getgameProxy();
}
