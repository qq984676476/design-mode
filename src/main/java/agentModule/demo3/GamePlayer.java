package agentModule.demo3;

public class GamePlayer implements IGamePlayer {
    private String name;
    public void loggin() {
        System.out.println(this.name + "登录");
    }

    public void killBoss() {
        System.out.println(this.name + "打怪");
    }

    public void upGrade() {
        System.out.println(this.name + "升级");
    }

    public  GamePlayer(IGamePlayer _gamePlayer, String name) throws Exception {
        if(_gamePlayer == null){
            throw new Exception("不能创建");
        }
        this.name = name;
    }
}
