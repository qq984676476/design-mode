package agentModule.demo3;

public class GamePlayerProxy implements IGamePlayer {
    private IGamePlayer player;
    public void loggin() {
        this.player.loggin();
    }

    public void killBoss() {
        this.player.killBoss();
    }

    public void upGrade() {
        this.player.upGrade();
    }

    public GamePlayerProxy(String name){
        try {
            player = new GamePlayer(this, name);//客户场景并不知道这里有代理
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
