package buildModule.demo1;

import java.util.ArrayList;

/**
 * 建造者模式
 */
public class Cline {
    public static void main(String[] args){
        HummerModel H1 = new HummerH1();
        ArrayList<String> sequence = new ArrayList<String>();
        sequence.add("start");
        sequence.add("alarm");
        sequence.add("engineBoom");
        sequence.add("stop");
        H1.setSequncece(sequence);
        H1.run(sequence);

        sequence.clear();
        HummerModel H2 = new HummerH2();
        sequence.add("alarm");
        sequence.add("start");
        sequence.add("engineBoom");
        sequence.add("stop");
        H2.run(sequence);
    }
}
