package buildModule.demo1;

import java.util.ArrayList;
import java.util.List;

public abstract class HummerModel {
    List<String> sequncece = new ArrayList<String>();
    abstract void start();
    abstract void stop();
    abstract void alarm();
    abstract void engineBoom();
    void run(List<String> sequncece) {
        for (String action : sequncece) {
            if(action.equals("start")){
                this.start();
            }
            if(action.equals("stop")){
                this.stop();
            }
            if(action.equals("alarm")){
                this.alarm();
            }
            if(action.equals("engineBoom")){
                this.engineBoom();
            }
        }
    }

    final public void setSequncece(List<String> sequncece){
        this.sequncece = sequncece;
    }
}
