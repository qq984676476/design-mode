package buildModule.demo1;

import java.util.ArrayList;
import java.util.List;

public class HummerH1 extends HummerModel {
    void start() {
        System.out.println("H1启动");
    }

    void stop() {
        System.out.println("H1停止");
    }

    void alarm() {
        System.out.println("H1鸣笛");
    }

    void engineBoom() {
        System.out.println("H1轰鸣");
    }

}
