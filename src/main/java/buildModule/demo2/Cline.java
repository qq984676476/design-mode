package buildModule.demo2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 建造者模式的钩子方法
 */
public class Cline {
    public static void main(String[] args) throws IOException {
        HummerModel h1 = new HummerH1();
        h1.run();
        HummerH2 h2 = new HummerH2();
        h2.run();
    }
}
