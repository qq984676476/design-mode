package buildModule.demo2;

public abstract class HummerModel {
    abstract void start();
    abstract void stop();
    abstract void alarm();
    abstract void engineBoom();
    Boolean isAlarm(){
        return true;
    }
    //受外界影响的方法叫做钩子方法
    void run() {
        this.start();
        this.alarm();
        this.engineBoom();
        this.stop();
    }
}
