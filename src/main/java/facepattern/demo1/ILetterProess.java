package facepattern.demo1;

/**
 * 被装饰的类借口
 */
public interface ILetterProess {
    void wirteContexr(String context);

    void fileEnvelope(String addres);

    void letterIntoEnvelop();

    void sendLetter();
}
