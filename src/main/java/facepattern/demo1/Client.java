package facepattern.demo1;

/**
 * 门面模式
 */
public class Client {
    public static void main(String[] args){
        ModenPostOfffice modenPostOfffice = new ModenPostOfffice();
        String address = "地址";
        String context = "内容";
        modenPostOfffice.sendLetter(context, address);

    }
}
