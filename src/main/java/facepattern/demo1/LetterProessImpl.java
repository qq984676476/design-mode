package facepattern.demo1;

/**
 * 被装饰的类
 */
public class LetterProessImpl implements ILetterProess {

    /**
     * 写信
     * @param context
     */
    public void wirteContexr(String context) {
        System.out.println("填写信的内容" + context);
    }

    /**
     * 写地址
     * @param addres
     */
    public void fileEnvelope(String addres) {
        System.out.println("填写收件人地址" + addres);
    }

    /**
     * 装进信封
     */
    public void letterIntoEnvelop() {
        System.out.println("把信放到信封");
    }

    /**
     * 发送
     */
    public void sendLetter() {
        System.out.println("发送");
    }
}
