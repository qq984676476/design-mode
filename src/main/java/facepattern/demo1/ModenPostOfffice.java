package facepattern.demo1;

/**
 * 封装类
 */
public class ModenPostOfffice {

    /**
     * 被封装的实例
     */
    private ILetterProess iLetterProess = new LetterProessImpl();
    private Police police = new Police();


    public void sendLetter(String context, String address){

        this.iLetterProess.wirteContexr(context);
        this.iLetterProess.fileEnvelope(address);
        this.police.checkLetter(iLetterProess);
        this.iLetterProess.letterIntoEnvelop();
        this.iLetterProess.sendLetter();

    }
}
