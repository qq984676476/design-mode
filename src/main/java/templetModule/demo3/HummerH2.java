package templetModule.demo3;

public class HummerH2 extends HummerModel {
    private boolean alarmFlag = true;
    void start() {
        System.out.println("H2启动");
    }

    void stop() {
        System.out.println("H2停止");
    }

    void alarm() {
        System.out.println("H2鸣笛");
    }

    void engineBoom() {
        System.out.println("H2轰鸣");
    }
    @Override
    Boolean isAlarm(){
        return this.alarmFlag;
    }

    public void setAlarmFlag(boolean alarmFlag){
        this.alarmFlag = alarmFlag;
    }

}
