package templetModule.demo3;

public class HummerH1 extends HummerModel {
    void start() {
        System.out.println("H1启动");
    }

    void stop() {
        System.out.println("H1停止");
    }

    void alarm() {
        System.out.println("H1鸣笛");
    }

    void engineBoom() {
        System.out.println("H1轰鸣");
    }

}
