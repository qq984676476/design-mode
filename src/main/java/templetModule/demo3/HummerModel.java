package templetModule.demo3;

public abstract class HummerModel {
    abstract void start();
    abstract void stop();
    abstract void alarm();
    abstract void engineBoom();
    Boolean isAlarm(){
        return true;
    }
    //受外界影响的方法叫做钩子方法
    void run() {
        this.start();
        if(this.isAlarm()){//外界条件改变影响模板方法执行，
            this.alarm();
        }
        this.engineBoom();
        this.stop();
    }
}
