package templetModule.demo3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Cline {
    public static void main(String[] args) throws IOException {
        HummerModel h1 = new HummerH1();
        h1.run();
        HummerH2 h2 = new HummerH2();
        String type = (new BufferedReader(new InputStreamReader(System.in))).readLine();
        if("0".equals(type)){
            h2.setAlarmFlag(false);
        }
        h2.run();
    }
}
