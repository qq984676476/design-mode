package templetModule.demo1;

public abstract class HummerModel {
    abstract void start();
    abstract void stop();
    abstract void alarm();
    abstract void engineBoom();
    void run() {
        this.start();
        this.alarm();
        this.engineBoom();
        this.stop();
    }
}
