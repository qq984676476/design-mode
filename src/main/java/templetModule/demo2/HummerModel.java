package templetModule.demo2;

public abstract class HummerModel {
    abstract void start();
    abstract void stop();
    abstract void alarm();
    abstract void engineBoom();
    abstract void run();//共同方法应提取到父类中
}
